import Ember from 'ember';
export default function() {
	// Using Plain JavaScript
	console.log("using plain Javascript");
	var base = {
		baseProperty: true
	};
	var derived = Object.create(base);
	console.log(derived.baseProperty); // true

	// Using Ember.js
	console.log("using Ember.js");
	base = Ember.Object.extend({
		baseProperty: true
	});

	derived = base.extend({
		derivedProperty: false
	});

	var derivedObject = derived.create();

	console.log(derivedObject.get('baseProperty')); // true
	console.log(derivedObject.get('derivedProperty')); // false
}