import Ember from 'ember';
export default function() {
	var base = Ember.Object.extend({
		baseProperty: true
	});

	var derived = base.extend({});

	var derivedObject = derived.create({
		derivedProperty: true
	});

	console.log(derivedObject.get('baseProperty')); // true
	console.log(derivedObject.get('derivedProperty')); // true

	var anotherDerivedObject = derived.create();
	console.log(anotherDerivedObject.get('derivedProperty')); // undefined
}